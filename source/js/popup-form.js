'use strict';

$(document).ready(function() {
  var desktopWidth = window.matchMedia("(min-width: 1279px)");

  function activateScroll() {
    $('html').removeClass('html--scroll');
    $('body').removeClass('body--scroll');
  }

  var closeSidebar = function () {
    $('.header__popup').removeClass('header__popup--show');
    $('.main-wrapper').removeClass('main-wrapper--header-show');
    // activateScroll();
  };

  // Сбрасывает поля формы и показывает сообщение об успешной отправке
  var resetForm = function (form) {
    var message = form.querySelector('.modal__wrapper--message');
    var bodyForm = form.querySelector('.modal__wrapper--main');

    form.reset();
    // $("input[type=tel]").val("+7 (");
    form.querySelector(".modal__phone--mask").classList.add('modal__phone--hidden');
    form.querySelector(".modal__phone--visible").classList.remove('modal__phone--not-empty');

    bodyForm.classList.add('modal__wrapper--hidden');
    message.classList.remove('modal__wrapper--hidden');
  };

  var dnResetForm = function (form) {
    var message = form.querySelector('.dn-modal__wrapper--message');
    var bodyForm = form.querySelector('.dn-modal__wrapper--main');

    form.reset();
    // $("input[type=tel]").val("+7 (");
    form.querySelector(".dn-modal__phone--mask").classList.add('dn-modal__phone--hidden');
    form.querySelector(".dn-modal__phone--visible").classList.remove('dn-modal__phone--not-empty');

    bodyForm.classList.add('dn-modal__wrapper--hidden');
    message.classList.remove('dn-modal__wrapper--hidden');
  };

  // Окно с сообщением - закрывается щелчком по ссылке (кнопке)
  $('.modal__message-close').click(function(e) {
    $('.modal__wrapper--message').addClass('modal__wrapper--hidden');
    $('.modal__wrapper--main').removeClass('modal__wrapper--hidden');
  });

  $('.dn-modal__message-close').click(function(e) {
    $('.dn-modal__wrapper--message').addClass('dn-modal__wrapper--hidden');
    $('.dn-modal__wrapper--main').removeClass('dn-modal__wrapper--hidden');
  });

  // Добавляет методы валидации для телефона
  $.validator.addMethod("minlenghtphone", function (value, element) {
      return value.replace(/\D+/g, '').length > 10;
    },
    "Неверный формат номера!");

  $.validator.addMethod("requiredphone", function (value, element) {
      return value.replace(/\D+/g, '').length > 1;
    },
    "Заполните это поле!");

  // Функция валидации форм
  var formValidate = function (form) {
    form.validate({
      rules: {
        name: {
          required: true,
          minlength: 3
        },
        email: {
          required: true,
          email: true
        },
        phone: {
          requiredphone: true,
          minlenghtphone: true,
          required: true
        }
      },
      messages: {
        name: {
          required: "Заполните это поле!",
          minlength: "Введите не менее 3 символов"
        },
        email: {
          required: "Заполните это поле!",
          email: "Неверный формат адреса"
        },
        phone: {
          required: "Заполните это поле!"
        }
      },
      submitHandler: function (form) {
        resetForm(form);
      }
    });
  };

  var dnFormValidate = function (form) {
    form.validate({
      rules: {
        name: {
          required: true,
          minlength: 3
        },
        email: {
          required: true,
          email: true
        },
        phone: {
          requiredphone: true,
          minlenghtphone: true,
          required: true
        }
      },
      messages: {
        name: {
          required: "Заполните это поле!",
          minlength: "Введите не менее 3 символов"
        },
        email: {
          required: "Заполните это поле!",
          email: "Неверный формат адреса"
        },
        phone: {
          required: "Заполните это поле!"
        }
      },
      submitHandler: function (form) {
        dnResetForm(form);
      }
    });
  };

  // ФОС на страницах
  formValidate($('#form-test'));
  formValidate($('#form-call'));
  formValidate($('#form-to'));

  // Модальное окно Тест-драйв
  window.modal.init($('.testdrive-click'),$('#popup1'),$('#btn-close1'),closeSidebar);
  dnFormValidate($('#popup1'));

  if (desktopWidth.matches) {
    var ps = new PerfectScrollbar('#popup1', {
      wheelSpeed: 0.5
    });
  }

  // Модальное окно ТО
  window.modal.init($('.to-click'),$('#popup2'),$('#btn-close2'),closeSidebar);
  dnFormValidate($('#popup2'));

  if (desktopWidth.matches) {
    var ps2 = new PerfectScrollbar('#popup2', {
      wheelSpeed: 0.5
    });
  }

  // Модальное окно Обратный звонок
  window.modal.init($('.calltext-click'),$('#popup3'),$('#btn-close3'),closeSidebar);
  dnFormValidate($('#popup3'));

  if (desktopWidth.matches) {
    var ps3 = new PerfectScrollbar('#popup3', {
      wheelSpeed: 0.5
    });
  }
});
