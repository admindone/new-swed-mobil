'use strict';

$(document).ready(function () {
  var TABLET_SIZE = 768;
  var FILTER_LIST = $('.filter__list');
  var filterBtns = document.querySelectorAll('.filter__checkbox');
  var filterAll = document.querySelector('#full');

  // Устанавливает фильтр "Все модели"
  if (filterAll) {
    filterAll.setAttribute('checked', 'checked');
    filterAll.checked = true;
  }

  // Функция проверки установленных фильтров
  // Возвращает false, если хотя бы один установлен
  var checkRemoveAllFilters = function () {
    var flag = true;
    [].forEach.call(filterBtns, function (element) {
      if (element != filterAll) {
        if (element.checked == true) {
          flag = false;
        }
      }
    });
    return flag;
  };

  // Переключает один из фильтров: если фильтр включается, сбрасывается "Все модели",
  // если выключается последним, устанавливает "Все модели"
  var onFilterClick = function (evt) {
    if (evt.target != filterAll) {
      if (evt.target.hasAttribute('checked')) {
        evt.target.removeAttribute('checked','checked');
        evt.target.checked = false;
        if (checkRemoveAllFilters()) {
          filterAll.setAttribute('checked', 'checked');
          filterAll.checked = true;
        }
      } else {
        evt.target.setAttribute('checked', 'checked');
        evt.target.checked = true;
        filterAll.removeAttribute('checked', 'checked');
        filterAll.checked = false;
      }
    }
  };

  // Переключает фильтр "Все модели".
  // Отключить нельзя. При включении сбрасываются все остальные фильтры
  var onFilterAllClick = function () {
    if (!filterAll.hasAttribute('checked')) {
      for ( var i = 1; i < filterBtns.length; i++ ) {
        if (filterBtns[i].hasAttribute('checked')) {
          filterBtns[i].removeAttribute('checked', 'checked');
          filterBtns[i].checked = false;
        }
      }
      filterAll.setAttribute('checked', 'checked');
    } else {
      if (!checkRemoveAllFilters()) {
        filterAll.removeAttribute('checked', 'checked');
        filterAll.checked = false;
      } else {
        filterAll.checked = true;
      }
    }
  };

  // Устанавливает прослушивание события клика на фильтры
  [].forEach.call(filterBtns, function (element) {
    element.addEventListener('click', onFilterClick);
  });

  // Устанавливает прослушивание события клика на фильтр "Все модели"
  if (filterAll) {
    filterAll.addEventListener('click', onFilterAllClick);
  }

  // Слайдер
  FILTER_LIST.flickity({
    cellAlign: 'left',
    wrapAround: true,
    freeScroll: true,
    draggable: true,
    contain: true,
    watchCSS: true,
    prevNextButtons: false,
    pageDots: false
  });

});
