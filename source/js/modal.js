'use strict';

(function () {
  var desktopWidth = window.matchMedia("(min-width: 1279px)");

  // Проверяет ширину попапа
  function checkWidthPopup(popup) {
    return popup.outerWidth() === $(window).width();
  }

  // Запрещает скролл страницы
  function OffScroll () {
    var winScrollTop = $(window).scrollTop();
    $(window).bind('scroll',function () {
      $(window).scrollTop(winScrollTop);
    });
  }

  function initModal(link, popup, classCloseBtn, closeCallBack) {
    // Открывает попап
    link.click(function(e) {
      e.preventDefault();
      popup.show();

      if (checkWidthPopup(popup)) {
        OffScroll();
      }
    });

    $(classCloseBtn, popup).click(function(e) {
      e.preventDefault();
      popup.hide();

      if (closeCallBack) {
        closeCallBack(e);
      }
      $("body").css("overflow", "auto");
      if (checkWidthPopup(popup)) {
        $(window).unbind('scroll');
      }
    });

    // закрытие при клике вне попапа
    if (desktopWidth.matches) {
      $(document).mouseup(function (e){
        if (!popup.is(e.target)
          && popup.has(e.target).length === 0) {
          popup.hide();
        }
        $("body").css("overflow", "auto");
        if (checkWidthPopup(popup)) {
          $(window).unbind('scroll');
        }
      });
    }
  }

  window.modal = {
    init: initModal
  };
})();
