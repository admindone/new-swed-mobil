'use strict';

$(document).ready(function (e) {
  var desktopWidth = window.matchMedia("(min-width: 1279px)");
  window.flagColorMove = true;

  // Подготовка к показу в слайдере авто с выбранным цветом
  $('.version').each(function (i, el) {
    var complectColor = $(el).find('.version__change-wrapper .version__change-color--active').attr('data-color');
    $(el).find('picture[data-color =' + complectColor + ']').css('display', 'block');
  });

  // Инициализация слайдера с авто в ЦА
  $('.version__slider').slick({
    mobileFirst: true,
    dots: true,
    arrows: false,
    slidesToShow: 1,
    responsive: [
      {
        breakpoint: 1279,
        settings: {
          arrows: true,
          fade: true,
          centerMode: true
        }
      }
    ]
  });


  // Запрещает скролл страницы
//  function OffScroll () {
//    var winScrollTop = $(window).scrollTop();
//    $(window).bind('scroll',function () {
//      $(window).scrollTop(winScrollTop);
//    });
//  }

  // Замена скролла
  var versionOptions = [].slice.call(document.querySelectorAll('.version__content-cover'));
  versionOptions.forEach(function (elem) {
    var ps = new PerfectScrollbar(elem, {
      wheelSpeed: 0.5
    });
/*    if (desktopWidth.matches) {
      elem.addEventListener('mouseover', function (e) {
        OffScroll();
      });
      elem.addEventListener('mouseout', function (e) {
        $(window).unbind('scroll');
      });
    } */
  });

  // Реакции на события:
  // Переключение цвета авто
  $('.version__change-color').click(choiceColor);

  // Переключение карточек целевых аудиторий
  $('.complectation__btn').click(choiceComplectation);

  // Переключение целевых аудиторий Престиж-Комфорт
  $('.version__toggle').click(toggleComplectation);

  // Кнопка "Ракурсы и интерьер"
  $('.version__btn-open').click(removePreview);

  // Кнопка "Назад" (возвращает превью на слайдер ракурсов)
  $('.version__btn-close').click(function () {
    $('.version.current').find('.version__slider').slick('slickGoTo', 0);
    $(this).hide();
    $('.version__btn-open').show();
  });

  // Кнопка "Записаться на тест-драйв"
  $('.version__btn').click(function () {
    var testDriveOffset = $('.new-auto .test-drive').offset().top;
    if (desktopWidth.matches) {
      $('html, body').animate({ scrollTop: testDriveOffset - 200 }, 'slow');
    } else {
      $('html, body').animate({ scrollTop: testDriveOffset - 55 }, 'slow');
    }
  });

  // Вешает модификатор для слайдов интерьера
  // (6 и 7 слайды, для 0 - убираем табы и кнопки переключения цвета)
  $('.version.current').on('beforeChange', beforeChangeHandler);

  // Дополнительная информация по опциям
  $('.version__item').click(moreInfo);

  // Закрытие попапа при нажатии на крестик
  $('.version__info-close').click(function (e) {
    $('.version__info').fadeOut(300);
    $('.version__item-more').removeClass('version__item-more--active');
    $('body').css('overflow', 'auto');
  });

  // Закрытие при клике вне попапа
  $(document).mouseup(function (e){
    if (desktopWidth.matches) {
      if (!$('.version__info').is(e.target)
        && $('.version__info').has(e.target).length === 0) {
        $('.version__info').fadeOut(300);
        if (!$('.version__item').has(e.target).length === 0) {
          $('.version__item-more').removeClass('version__item-more--active');
        }
      }
    }
  });

});

/* ********************************************** */
/* Функции обработки событий */

// Клик на "Ракурсы и интерьер" переходит на второй слайд
function removePreview() {
  var desktopWidth = window.matchMedia("(min-width: 1279px)");

  $('.version.current').find('.version__slider').slick('slickGoTo',1);
  reinitSlider();
  if (desktopWidth.matches) {
    $('.version__btn-close').show();
  }
  $('.version__btn-open').hide();
}

// Срабатывает до смены слайда в слайдере
function beforeChangeHandler(event, slick, currentSlide, nextSlide) {
  modifySlide(nextSlide);
}

// Модификаторы для нулевого(превью) и на 6 и 7 слайды
function modifySlide(slideId) {
  var desktopWidth = window.matchMedia("(min-width: 1279px)");

  if (slideId === 0) {
    $('.version.current').find('.version__slider').addClass('version__slider--hide');
    $('.version.current').find('.version__change-wrapper').hide();
    $('.version.current').find('.version__slider').addClass('version__slider--black');
    $('.version__btn-close').hide();
    $('.version__btn-open').show();
  } else {
    $('.version.current').find('.version__slider').removeClass('version__slider--hide');
    $('.version.current').find('.version__change-wrapper').show();
    if (desktopWidth.matches) {
      $('.version__btn-close').show();
    }
    $('.version__btn-open').hide();
    if (slideId === 6 || slideId === 7) {
      $('.version__btn-close').addClass('version__btn-close--black');
      $('.version.current').find('.version__slider').addClass('version__slider--black');
    } else {
      $('.version.current').find('.version__slider').removeClass('version__slider--black');
      $('.version__btn-close').removeClass('version__btn-close--black');
    }
  }
}

// скроллинг до выезжающего блока
function scrollComplectation() {
  var DESKTOP_MENU_HEIGHT = 55;
  var QUOTES_HEIGHT = 63;
  var desktopWidth = window.matchMedia("(min-width: 1279px)");

  if (desktopWidth.matches) {
    var offsetCompletationDesktop = $('.complectation__promise').position().top;
    $('html, body').animate({scrollTop: offsetCompletationDesktop - DESKTOP_MENU_HEIGHT - QUOTES_HEIGHT}, 'slow');
  } else {
    var offsetCompletationDesktop = $('.complectation__promise').position().top;
    $('html, body').animate({scrollTop: offsetCompletationDesktop - DESKTOP_MENU_HEIGHT}, 'slow');
  }
}

function viewComplectation(selector, title, promise) {
  $('.complectation__title--blue').text(title);
  $('.complectation__promise').text(promise);
  $('.version__caption--blue').text(title);
  $('.complectation__title').fadeIn('slow');
  $('.complectation__promise').fadeIn('slow');
  $('.version__caption').fadeIn('slow');

  // Переключаем ЦА в модальной форме
  $('.modal--drive').find('select.modal__ofice').find('option').removeAttr('selected', 'selected');
  $('.modal--drive').find('select.modal__ofice option[data-version="' + selector + '"]')
    .attr('selected', 'selected')
    .trigger('refresh');

  var block = $('.version[data-version="' + selector + '"]'); // Селект нужного блока
  var slideId = currentSlide(); // id слайда
  var changeColor = $('.version.current').find('.version__change-wrapper .version__change-color--active').attr('data-color');

  $('.version.current').removeClass('current');
  $('.version').hide();

  block.find('.version__change-wrapper .version__change-color--active').each(function (i, el) {
    if($(el).attr('data-color') != changeColor) {
      window.flagColorMove = false;
      $(el).click();
    }
    window.flagColorMove = true;
  });

  block.css('display', 'flex');
  block.addClass('current');

  $('.version').off('beforeChange', beforeChangeHandler); // Удаляет слушатель смены слайда
  $('.version.current').on('beforeChange', beforeChangeHandler); // Добавляет слушатель смены слайда
  $('.version.current').find('.version__slider').addClass('version__slider--hide');
  $('.version.current').find('.version__change-wrapper').hide();
  $('.version.current').find('.version__slider').addClass('version__slider--black');
  $('.version__btn-close').hide();
  $('.version__btn-open').show();
  setSlide(0); // Устанавливает положение слайдера
  reinitSlider(); // Реиницализация слайдера
}

// Функция переключения комплектаций Престиж-Комфорт
function toggleComplectation() {
  var selector = $(this).val();

  $('.version.current').find('.version__toggle').each(function (i, el) {
    $('.version__toggle').toggleClass('version__toggle--active');
  });

  // Получаем заголовок и "обещание" для выбранной целевой аудитории
  var title = $(this).data().title;
  var promise = $(this).data().promise;
  viewComplectation(selector, title, promise);
};

// Функция выбора комплектации
function choiceComplectation() {
  var selector = $(this).val();
  var flag = $(this).hasClass('complectation__btn--active');

  $('.complectation__btn').removeClass('complectation__btn--active');
  $('.complectation__toggle').text("Выбрать");

  if (!flag) {
    $(this).addClass('complectation__btn--active');
    $(this).find('.complectation__toggle').text("Отменить");

    // Получаем заголовок и "обещание" для выбранной целевой аудитории
    var title = $(this).data().title;
    var promise = $(this).data().promise;
    viewComplectation(selector, title, promise);
    scrollComplectation();
    $('.version-container').fadeIn('slow');
  } else {
    $('.complectation__title').fadeOut('slow');
    $('.complectation__promise').fadeOut('slow');
    $('.version__caption').fadeOut('slow');
    $('.version-container').fadeOut('slow');
  }
};

// Реиницализация слайдера
function reinitSlider() {
  $('.version.current').find('.version__slider').slick('refresh');
};

// Возвращается id слайда
function currentSlide() {
  var curSlide = $('.version.current').find('.version__slider').slick('slickCurrentSlide');
  return curSlide;
};

// Устанавливает положение слайдера
function setSlide(id) {
  $('.version.current').find('.version__slider').slick('slickGoTo', id, false);
};

// Доп информация (попап) по опции
function moreInfo() {
  if ($(this).has('.version__item-more').length === 0) {
    return;
  }

  if ($(this).find('.version__item-more').hasClass('version__item-more--active')) {
    $('.version__item-more').removeClass('version__item-more--active');
    $('.version__info').fadeOut(300);
    $('body').css('overflow', 'auto');
  } else {
    var move = 0;
    move = $(this).find('.version__item-more').data().move;
    var id = $(this).find('.version__item-more').data().more;
    var info = $(this).parents('.version.current').find('.version__info*[data-more="' + id + '"]');
    var desktopWidth = window.matchMedia("(max-width: 1279px)");

    $('.version__item-more').removeClass('version__item-more--active');
    $(this).find('.version__item-more').addClass('version__item-more--active');

    $('.version__info').fadeOut();

    if (desktopWidth.matches) {
      $('body').css('overflow', 'hidden');
    }

    info.fadeIn(300);
  }
}

function elemScrollHeght(elm) {
  elm.scrollTop(elm.get(0).scrollHeight);
  var scrollHeight = elm.scrollTop() + elm.height();
  elm.scrollTop(0);

  return scrollHeight;
}

// function elemScrollHeght2(elm) {
//   var iHeight = elm.height();
//   var iScrollHeight = elm.prop("scrollHeight");
//
//   return iHeight;
// }

function choiceColor(e) {
  var MOBILE_MENU_HEIGHT = 55;
  var desktopWidth = window.matchMedia("(min-width: 1279px)");
  var id = currentSlide();
  var color = $(this).data().color;

  $('.version__change-color*[data-color="' + color + '"]').addClass('version__change-color--active')
    .siblings('.version__change-color').removeClass('version__change-color--active');
  $('.version__slider picture[data-color="'+ color + '"]').css('display', 'block')
    .siblings('picture').hide();

  if (id === 0) {
    $('.version.current').find('.version__slider').slick('slickGoTo', 1);
  }

  // Убираем превью
  if (!desktopWidth.matches) {
//    $('.version.current').find('.version__preview').fadeOut('slow');
//  } else {
//    if (id === 0) {
//      $('.version.current').find('.version__slider').slick('slickGoTo', 1);
//    }
    // скроллинг до ракурсов
    if (window.flagColorMove) {
      var offsetMobileVersion = $('.version.current').find('.version__slider').offset().top;
      $('html, body').animate({scrollTop: offsetMobileVersion - MOBILE_MENU_HEIGHT}, 'slow');
    }
  }
}

// Дебаунс
function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this, args = arguments;
    var later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};
