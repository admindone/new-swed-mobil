'use strict';

$(document).ready(function () {

  // Проверка устройства
  var Device = new (function() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    // Windows Phone must come first because its UA also contains "Android"
    this.windows = /windows phone/i.test(userAgent);
    this.android = /android/i.test(userAgent);
    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    this.ios = /iPad|iPhone|iPod/.test(userAgent) && !window.MSStream
  })();

  var winScrollTop;

  // Скрывает меню
  function hidePopupMenu() {
    $('.dn-header__popup').removeClass('dn-header__popup--show');
    $('.main-wrapper').removeClass('main-wrapper--header-show');

    $("body").css("overflow", "auto");

    // if(Device.ios){
    //   $(window).scrollTop(winScrollTop);
    //   $('body').removeClass('body--scroll');
    // } else {
    //   $("body").css("overflow", "auto");
    // }
  }

  // Клик по бургеру
  $('#burger').click(function (e) {
    e.preventDefault();
    $('.dn-header__popup').addClass('dn-header__popup--show');
    $('.main-wrapper').addClass('main-wrapper--header-show');

    $("body").css("overflow", "hidden");

    // if(Device.ios){
    //   $('body').addClass('body--scroll');
    //   $('body').css('padding-top', '0');
    // } else {
    //   $("body").css("overflow", "hidden");
    // }
  });

  // Клик по крестику
  $('.dn-header__popup-close').click(hidePopupMenu);

  // Клик вне сайдбара
  $('.main-wrapper').click(hidePopupMenu);

  // Выпадающий список в сайдбаре
  $('.dn-header__popup-division--drop-down').click(function (e) {
    e.preventDefault();
    $(this).next().slideToggle(300);
    $(this).find('.dn-header__popup-arrow').toggleClass('dn-header__popup-arrow--rotate');
  });

  // Клик по гамбургеру Десктоп
  var burgerClicked = false;

  $('.dn-header__nav-drop-menu-burger').click(
    function burgerSlide(e) {
      if (!burgerClicked) {
        $('.dn-header__nav-list--burger').slideDown(300);
        burgerClicked = true;
      } else {
        $('.dn-header__nav-list--burger').slideUp(300);
        burgerClicked = false;
      }
    }
  );

  // Закрытие при клике вне гамбургера
  $(document).click(function (e) {
    var div = $(".dn-header__nav-drop-menu-burger");
    if (!div.is(e.target)
      && div.has(e.target).length === 0) {
      $('.dn-header__nav-list--burger').slideUp(300);
      burgerClicked = false;
    }
  });

  // Фикс меню
  var topOfNav = $('.dn-header__nav').offset().top;
  var headerHeight = $('.dn-header').height();

  function fixNav() {
    if ($('.dn-header__popup').hasClass('dn-header__popup--show')) {
      return;
    }

    if (window.scrollY >= topOfNav) {
      winScrollTop = $(window).scrollTop();
      $('body').css('padding-top', headerHeight + 'px');
      $('.dn-header__nav-color-wrapper').addClass('dn-header__nav--fixed');
      $('.dn-header__top').addClass('dn-header__top--hide');

    } else {
      $('body').css('padding-top', '0');
      $('.dn-header__nav-color-wrapper').removeClass('dn-header__nav--fixed');
      $('.dn-header__top').removeClass('dn-header__top--hide');
    }
  }

  window.addEventListener('scroll', fixNav);


  // Модельный ряд
  var autoClicked = false;

  $('.auto-link').click(function (e) {
    e.preventDefault();
    $('.dn-auto').slideToggle(800);
    autoClicked = true;
    $('.auto-link .dn-header__nav-arrow').toggleClass('dn-header__nav-arrow--rotate');
  });

  // Клик вне модельного ряда
  $(document).click(function (e) {
    if (autoClicked) {
      var div = $('.dn-auto');
      var link = $('.auto-link');
      var svg = $('.auto-link .dn-header__nav-arrow');

      if (!div.is(e.target) &&
        div.has(e.target).length === 0 &&
        !link.is(e.target) && !svg.is(e.target)
      ) {
        $('.dn-auto').slideUp(800);
        autoClicked = false;
        $('.auto-link .dn-header__nav-arrow').removeClass('dn-header__nav-arrow--rotate');
      };
    };
  });


  // Сервис
  $('.service-link').click(function (e) {
    $(this).find('ul').slideToggle(300);
    $(this).find('.dn-header__nav-arrow').toggleClass('dn-header__nav-arrow--rotate');
  });
});
