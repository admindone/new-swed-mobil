'use strict';

(function () {
  var version = [].slice.call(document.querySelectorAll('.version'));

  // *************** Временно скрыто по требованию заказчика *********** //
  // var equipmentFunction = [].slice.call(document.querySelectorAll('.equipment__function-item'));
  // *************** --------------------------------------- *********** //

  var desktopWidth = window.matchMedia("(min-width: 1279px)");

  // Обработка открытия/закрытия стандартных функций
  version.forEach(function (value) {
    var link = value.querySelector('.version__item--equipment');
    var popup = value.querySelector('.equipment');
    var popupWrapper = popup.querySelector('.equipment-wrapper');
    var popupItem = [].slice.call(popup.querySelectorAll('.equipment__function'));

    // Тонкий скролл от мака
    if (desktopWidth.matches) {
      var ps = new PerfectScrollbar(popupWrapper, {
        wheelSpeed: 0.5
      });
    }

    var popupJq = $(popup);
    var linkJq = $(link);

    window.modal.init(linkJq, popupJq, '.equipment__close');

    // Определение высоты блока и номера последней опции в 1 колонке
    function calculateLastItemColumn(items, popupBlock) {
      var sumHeight = 0;
      var resultHeight = 0;
      var lastItemColumn = 0;

      if (items.length === 1) {
        sumHeight = items[0].clientHeight + 1;
      } else {
        for (var i = 0; ; i++) {
          if (sumHeight > popupBlock.clientHeight / 2) {
            break;
          }

          sumHeight += items[i].clientHeight;
          // Добавляем 1px к высоте каждой опции для верхнего бордера
          resultHeight += items[i].clientHeight + 1;
          lastItemColumn = i;
        }
      }

      return {
        resultHeight: resultHeight,
        lastItemColumn: lastItemColumn
      }
    }

    // При открытии попапа на десктопе - вычисляем высоту каждого блока с опциями
    // (опции располагаются всегда в 2 столбца)
    // Высота равна высоте половины опций.
    if (desktopWidth.matches) {
      link.addEventListener('click', function (evt) {
        popupItem.forEach(function (value2) {
          var functionItem = value2.querySelectorAll('.equipment__function-item');

          var blockParams = calculateLastItemColumn(functionItem, value2);

          var equipmentFunctionWrapper = value2.querySelector('.equipment__function-wrapper');
          equipmentFunctionWrapper.style.height = blockParams.resultHeight + 1 + 'px';
          // На последнюю опцию в 1м столбце - добавляем нижний бордер
          functionItem[blockParams.lastItemColumn].style.borderBottom = "1px solid #d4d1bd";
          // На последнюю опцию вообще также добавляем нижний бордер
          functionItem[functionItem.length - 1].style.borderBottom = "1px solid #d4d1bd";
        });
      })
    }

    $('.equipment__close').on('click', function(){
      $('.equipment__function-wrapper').removeAttr('style');
    });
  });

  // *************** Временно скрыто по требованию заказчика *********** //

  // Обработка открытия/закрытия конкретной функции

  // equipmentFunction.forEach(function (value) {
  //   var ps = new PerfectScrollbar(value, {
  //     wheelSpeed: 0.5
  //   });
  //
  //   var linkFunction = value.querySelector(".equipment__function-item-title");
  //   var popupFunction = value.querySelector('.equipment__popup-item');
  //   var equipmentBtnClose = value.querySelector('.equipment__popup-close');
  //
  //   equipmentBtnClose.addEventListener('click', function (evt) {
  //     popup.style.display = "none";
  //   });
  //
  //   // Обработка ссылки "Назад" со снятием активного пункта
  //   linkFunction = $(linkFunction);
  //   popupFunction = $(popupFunction);
  //   window.modal.init(linkFunction, popupFunction, '.equipment__link', function () {
  //     value.querySelector(".equipment__function--more").classList.remove("version__item-more--active");
  //   });
  // });

  // *************** --------------------------------------- *********** //
})();
