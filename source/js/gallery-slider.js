$(document).ready(function () {
  var DESKTOP_SIZE = 1280;
  var PHOTO_LIST = $('.photos__wrapper');
  var PHOTO_ITEM = $('.photos__item');
  var documentWidth = document.documentElement.clientWidth;

  function isSlickInitialized() {
    return PHOTO_LIST.hasClass('slick-initialized');
  }

  function initSlider() {
    if (PHOTO_LIST.length !== 0 && !isSlickInitialized()) {
      PHOTO_LIST.slick({
        dots: false,
        arrows: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true,
        centerMode: true
      });
    }
  }

  if ($(window).width() < DESKTOP_SIZE) {
    initSlider();
  } else {
    PHOTO_ITEM.width((document.documentElement.clientWidth - 20)/3);
  }

  $(window).resize(function () {
    if ($(window).width() >= DESKTOP_SIZE) {
      if (isSlickInitialized()) {
        PHOTO_LIST.slick('unslick');
      }
      PHOTO_ITEM.width((document.documentElement.clientWidth - 20)/3);
    } else {
      initSlider();
    }
  })
});
