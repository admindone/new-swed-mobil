$(document).ready(function () {
  var module = document.querySelectorAll(".offers__text");

  $('.offers__list').slick({
    accessibility: !1,
    appendDots: ".offers",
    arrows: !1,
    autoplay: !0,
    autoplaySpeed: 4e3,
    dots: true,
    fade: true,
    dotsClass: "offers__dots",
    mobileFirst: !0,
    speed: 500,
    pauseOnFocus: true,
    pauseOnHover: false,
    responsive: [{
      breakpoint: 1280,
      settings: {
        arrows: !0,
        prevArrow: $(".offers__arrow--prev"),
        nextArrow: $(".offers__arrow--next")
      }
    }]
  });

  $('.offers__arrow').click(function() {
    $('.offers__list').slick('slickPause');
  });

  $('.offers__dots button').click(function() {
    $('.offers__list').slick('slickPause');
  });

});
