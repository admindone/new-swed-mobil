var ADDRESS_PRIMORSKI = {lat: 59.983705, lng: 30.236928};
var ADDRESS_ENERGETIC = {lat: 59.975308, lng: 30.443565};
var ADDRESS_ZHUKOVA = {lat: 59.861060, lng: 30.233324};

var STYLES = [
  {
    "stylers":[{"saturation":-100}]
  }
  ];

var map, map2, map3;
var image = './icons/icon-geo.svg';


var optionsEnergetic = {
  center: ADDRESS_ENERGETIC,
  zoom: 17,
  mapTypeControl: false,
  fullscreenControl: true,
  styles: STYLES
};

var optionsPrimorski = {
  center: ADDRESS_PRIMORSKI,
  zoom: 17,
  mapTypeControl: false,
  fullscreenControl: true,
  styles: STYLES
};

var optionsZhukova = {
  center: ADDRESS_ZHUKOVA,
  zoom: 17,
  mapTypeControl: false,
  fullscreenControl: true,
  styles: STYLES
};

function getLocationParameters(lat, lng) {
  return new google.maps.LatLng(lat, lng);
}

function initMap() {
  map = new google.maps.Map(document.getElementById("map-primorski"),
    optionsPrimorski);

  map2 = new google.maps.Map(document.getElementById("map-energetic"),
    optionsEnergetic);

  map3 = new google.maps.Map(document.getElementById("map-zhukova"),
    optionsZhukova);

  // google.maps.event.addListener(marker, 'click', function() {
  //   document.location='https://www.swedmobil.ru/contacts#location1';
  // });

  var features = [
    {
      position: getLocationParameters(59.983705, 30.236928),
      type: 'primorski',
      map: map,
      location: 'https://www.swedmobil.ru/contacts#location1'
    }, {
      position: getLocationParameters(59.975308, 30.443565),
      type: 'energetic',
      map: map2,
      location: 'https://www.swedmobil.ru/contacts#location2'
    }, {
      position: getLocationParameters(59.861060, 30.233324),
      type: 'zhukova',
      map: map3,
      location: 'https://www.swedmobil.ru/contacts#location3'
    }
    ];

  // Create markers.
  features.forEach(function(feature) {
    var marker = new google.maps.Marker({
      position: feature.position,
      icon: image,
      map: feature.map
    });
  });
}
