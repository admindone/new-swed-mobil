'use strict';

(function () {
  if (document.querySelector('.advantage__item')) {
    var advantageItem = [].slice.call(document.querySelectorAll('.advantage__item'));
    var btnShowAdvantages = document.querySelector('#btnAdvantage');
    var CLASS_HIDDEN_ADVANTAGE = 'advantage__item--hide';
    var CLASS_HIDDEN_BTN = 'advantage__btn--hide';

    // Инициализация попапов блока преимуществ
    advantageItem.forEach(function (value) {
      var link = value.querySelector('.advantage__link');
      var popup = value.querySelector('.advantage__popup');
      popup = $(popup);
      link = $(link);
      window.modal.init(link, popup, '.jsBtnClose');

    });

    btnShowAdvantages.addEventListener('click', function () {
      advantageItem.forEach(function (value) {
        if (value.classList.contains(CLASS_HIDDEN_ADVANTAGE)) {
          value.classList.remove(CLASS_HIDDEN_ADVANTAGE);
        }
      });
      btnShowAdvantages.classList.add(CLASS_HIDDEN_BTN);
    });
  }
})();
