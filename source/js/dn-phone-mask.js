'use strict';
var DN_HIDDEN_MASK = '+7 (___) ___-__-__';
var dnInputAll = [].slice.call(document.querySelectorAll('.dn-modal__input-box--tel'));


dnInputAll.forEach(function (value) {
  var dnInput = value.querySelector(".dn-modal__phone--visible");
  var dnMask = value.querySelector(".dn-modal__phone--mask");

  function dnClickPhoneHandler() {
    dnMask.classList.remove("dn-modal__phone--hidden");
  }

  dnInput.addEventListener('click', dnClickPhoneHandler);

  var dnCleave = new Cleave(dnInput, {
    blocks: [3, 3, 3, 2, 2],
    delimiters: ['(', ') ', '-', '-'],
    uppercase: true,
    prefix: '+7 ',
    numericOnly: true
  });

  function dnInputPhoneHandler(evt) {
    var newValue = dnInput.value;
    dnMask.value = newValue + DN_HIDDEN_MASK.substr(newValue.length);

    if (evt.data !== null && Number.isInteger(+evt.data)) {
      if (dnInput.value.length <= 5) {
        dnInput.selectionStart = 5;
      } else if (dnInput.selectionStart < 4) {
        dnInput.value = '+7 (' + evt.data + dnInput.value.slice(4, dnInput.value.length);
      }
    }
  }

  dnInput.addEventListener('blur', function () {
    if (dnInput.value) {
      dnInput.classList.add("dn-modal__phone--not-empty");
    }
  });

  dnInput.addEventListener('input', dnInputPhoneHandler);
});



