'use strict';
var HIDDEN_MASK = '+7 (___) ___-__-__';
var inputAll = [].slice.call(document.querySelectorAll('.modal__input-box--tel'));


inputAll.forEach(function (value) {
  var input = value.querySelector(".modal__phone--visible");
  var mask = value.querySelector(".modal__phone--mask");

  function clickPhoneHandler() {
    mask.classList.remove("modal__phone--hidden");
  }

  input.addEventListener('click', clickPhoneHandler);

  var cleave = new Cleave(input, {
    blocks: [3, 3, 3, 2, 2],
    delimiters: ['(', ') ', '-', '-'],
    uppercase: true,
    prefix: '+7 ',
    numericOnly: true
  });

  function inputPhoneHandler(evt) {
    var newValue = input.value;
    mask.value = newValue + HIDDEN_MASK.substr(newValue.length);

    if (evt.data !== null && Number.isInteger(+evt.data)) {
      if (input.value.length <= 5) {
        input.selectionStart = 5;
      } else if (input.selectionStart < 4) {
        input.value = '+7 (' + evt.data + input.value.slice(4, input.value.length);
      }
    }
  }

  input.addEventListener('blur', function () {
    if (input.value) {
      input.classList.add("modal__phone--not-empty");
    }
  });

  input.addEventListener('input', inputPhoneHandler);
});



