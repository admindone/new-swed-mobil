'use strict';

$(document).ready(function () {

  $('.promo__link').click(function (e) {
    e.preventDefault();
    var offsetComplect = $('.complectation').offset().top;
    $('html, body').animate({scrollTop: offsetComplect - 55}, 'slow');
  });
});
