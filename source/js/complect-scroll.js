(function() {
  if (document.querySelector('.complectation__choice-desktop')) {
    var choice = document.querySelector('.complectation__choice-desktop');
    var padding = 15;

    //Управляет видимостью градиента в блоке комплектаций

    var gradientShow = function() {
      var container = document.querySelector('.version.current');
      var request = container.querySelector('.version__request');
      var scroll = container.querySelector('.ps__thumb-y');
      var fill = container.querySelector('.version__fill');
      var footer = container.querySelector('.version__footer');

      var fillBottom = window.pageYOffset + fill.getBoundingClientRect().bottom;
      var footerTop = window.pageYOffset + footer.getBoundingClientRect().top;


      if(fillBottom - footerTop > padding) {
        footer.classList.add('version__footer--show');
      }

      var scrollHandler = function () {
        fillBottom = window.pageYOffset + fill.getBoundingClientRect().bottom;
        footerTop = window.pageYOffset + footer.getBoundingClientRect().top;

        if(fillBottom - footerTop <= padding) {
          footer.classList.remove('version__footer--show');
        } else {
          footer.classList.add('version__footer--show');
        }
      };

      container.addEventListener('mouseover', function (evt) {
        scrollHandler();
      });

      request.addEventListener('mousewheel', function (evt) {
        scrollHandler();
      });

      scroll.addEventListener('mousemove', function (evt) {
        scrollHandler();
      });
    };

    //Управление градиентом по клику на целевую аудиторию

    choice.addEventListener('click', function (evt) {
      gradientShow();
    });
  }
})();
