'use strict';

$(document).ready(function () {
  $('.offers__link').click(function (e) {
    e.preventDefault();
    var offsetVersion = $('.auto-info').offset().top;
    $('html, body').animate({scrollTop: offsetVersion - 55}, 'slow');
  });
});
