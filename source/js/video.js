if (document.querySelector(".video")) {
  var tag = document.createElement('script');
  tag.src = "https://www.youtube.com/iframe_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  var player;
  var video = document.querySelector(".video");
  var containerWidth = video.offsetWidth;
  var containerHeight = video.offsetHeight;
  var newPlayBtn = video.querySelector(".video__btn");
  var videoOverlay = video.querySelector(".video__overlay");

  function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
      host: 'https://www.youtube.com',
      height: containerHeight,
      width: containerWidth,
      videoId: video.dataset.youtubeId,
      playerVars: {
        iv_load_policy: 1,
        loop: 1,
        showinfo: 0,
        iv_load_policy: 3,
        rel: 0
      },
      events: {
        'onReady': onPlayerReady,
        'onStateChange': onPlayerStateChange
      }
    });
  }

  function onPlayerReady(event) {
    var targetYoutubeVideo = event.target;
    newPlayBtn.addEventListener("click", function(event) {
      targetYoutubeVideo.playVideo();
      videoOverlay.classList.add('video__overlay--hidden');
      newPlayBtn.classList.add('video__btn--hidden');
    });

    videoOverlay.addEventListener("click", function(event) {
      targetYoutubeVideo.playVideo();
      videoOverlay.classList.add('video__overlay--hidden');
      newPlayBtn.classList.add('video__btn--hidden');
    });

    window.addEventListener('scroll', function () {
      if (window.scrollY > video.offsetHeight + video.offsetTop) {
        player.pauseVideo();
      }
    });
  }

  function onPlayerStateChange(e) {
    if (e.data === YT.PlayerState.ENDED) {
      player.playVideo();
    }
  }
}
