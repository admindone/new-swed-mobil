"use strict";

var gulp = require("gulp");
var sass = require("gulp-sass");
var plumber = require("gulp-plumber");
var postcss = require("gulp-postcss");
var autoprefixer = require("autoprefixer");
var mqpacker = require("css-mqpacker");
var sourcemaps = require("gulp-sourcemaps");
var minify = require("gulp-csso");
var uglify = require("gulp-uglify");
var rename = require("gulp-rename");
var imagemin = require("gulp-imagemin");
var svgstore = require("gulp-svgstore");
var server = require("browser-sync").create();
var del = require("del");
var run = require("run-sequence");
var imageminJpegRecompress = require('imagemin-jpeg-recompress');
var concat = require('gulp-concat');
var notify = require('gulp-notify');
var watch = require('gulp-watch');

var listJsFiles = [
  'source/js/jquery.min.js',
  'source/js/jquery.formstyler.min.js',
  'source/js/jquery.maskedinput.js',
  'source/js/jquery.validate.js',
  'source/js/slick.min.js',
  'source/js/cleave.js',
  'source/js/flickity.pkgd.js',
  'source/js/modal.js',
  'source/js/header.js',
  'source/js/footer.js',
  'source/js/offers-slider.js',
  'source/js/filters-slider.js',
  'source/js/gallery-slider.js',
  'source/js/perfect-scrollbar.min.js',
  'source/js/complectation.js',
  'source/js/complect-scroll.js',
  'source/js/video.js',
  'source/js/advantage.js',
  'source/js/equipment.js',
  'source/js/myselect.js',
  'source/js/popup-form.js',
  'source/js/phone-mask.js',
  'source/js/promo.js',
  'source/js/map.js',
  'source/js/offer.js'
];

gulp.task("style", function () {
  gulp.src("source/styles/*.scss")
    .pipe(plumber({
      errorHandler: notify.onError(err => ({
        title: 'style',
        message: err.message
      }))
    }))
    .pipe(sass())
    .pipe(postcss([
      autoprefixer({
        browsers: [
          "last 1 version",
          "last 2 Chrome versions",
          "last 2 Firefox versions",
          "last 2 Opera versions",
          "last 2 Edge versions",
          "IE 11"
        ]
      }),
      mqpacker({
        sort: true
      })
    ]))
    .pipe(gulp.dest("build/css"))
    .pipe(minify())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest("build/css"))
    .pipe(server.reload({ stream: true }));
});

gulp.task("style_dev", function () {
  gulp.src("source/styles/style.scss")
    .pipe(plumber({
      errorHandler: notify.onError(err => ({
        title: 'style_dev',
        message: err.message
      }))
    }))
    .pipe(sass())
    .pipe(postcss([
      autoprefixer({
        browsers: [
          "last 1 version",
          "last 2 Chrome versions",
          "last 2 Firefox versions",
          "last 2 Opera versions",
          "last 2 Edge versions",
          "IE 11"
        ]
      }),
      mqpacker({
        sort: true
      })
    ]))
    .pipe(gulp.dest("source/css"))
    .pipe(minify())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest("source/css"))
    .pipe(server.reload({ stream: true }));
});

// Собирает, минифицирует и записывает в билд
// скрипты одним файлом
gulp.task("js", function () {
  gulp.src(listJsFiles)
    .pipe(plumber({
      errorHandler: notify.onError(err => ({
        title: 'js',
        message: err.message
      }))
    }))
    .pipe(concat('all.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest("build/js"))
    .pipe(server.reload({ stream: true }));
});

// Собирает скрипты в один файл в source
gulp.task('js_dev', function () {
  return gulp.src(listJsFiles)
    .pipe(plumber({
      errorHandler: notify.onError(err => ({
        title: 'js_dev',
        message: err.message
      }))
    }))
    .pipe(sourcemaps.init())
    .pipe(concat('all.min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('source/js'))
    .pipe(server.reload({ stream: true }));
});

// gulp.task("sprite", function () {
//   return gulp.src("source/img/{icon-*.svg,logo-*.svg}")
//     .pipe(svgstore({
//       inlineSvg: true
//     }))
//     .pipe(rename("sprite.svg"))
//     .pipe(gulp.dest("build/img"));
// });

gulp.task("images", function () {
  return gulp.src("source/img/**/*.{png,jpg,jpeg}")
    .pipe(plumber({
      errorHandler: notify.onError(err => ({
        title: 'images',
        message: err.message
      }))
    }))
    .pipe(imagemin([
      imageminJpegRecompress({
        progressive: true,
        max: 70,
        min: 65
      }),
      imagemin.optipng({ optimizationLevel: 5 }),
      imagemin.jpegtran({ progressive: true }),
      imagemin.svgo()
    ]))
    .pipe(gulp.dest("build/img"));
});

//gulp.task("html", function () {
//  return gulp.src("source/**/*.html")
//    .pipe(posthtml([
//      include()
//    ]))
//    .pipe(gulp.dest("build"));
//});

gulp.task("copy", function () {
  return gulp.src([
    "source/fonts/**/*.{woff,woff2}",
    "source/icons/*",
    "source/docs/*",
    "source/js/picturefill.min.js",
    "source/js/dn-phone-mask.min.js"
  ], {
      base: "source"
    })
    .pipe(plumber({
      errorHandler: notify.onError(err => ({
        title: 'images',
        message: err.message
      }))
    }))
    .pipe(gulp.dest("build"));
});

gulp.task("clean", function () {
  return del("build");
});

gulp.task("copyhtml", function () {
  return gulp.src([
    "source/*.html",
    "source/**/*.html"
  ], {
      base: "source"
    })
    .pipe(watch([
      "source/*.html",
      "source/**/*.html"
    ], {base: "source"}))
    .pipe(plumber({
      errorHandler: notify.onError(err => ({
        title: 'copyhtml',
        message: err.message
      }))
    }))
    .pipe(gulp.dest("build"))
    .pipe(server.reload({ stream: true }));
});

gulp.task("serve", function () {
  server.init({
    server: "build",
    notify: false,
    open: true,
    cors: true,
    ui: false
  });
  gulp.watch("source/styles/**/*.scss", ["style"]);
  gulp.watch("source/**/*.html", ["copyhtml"]);
  gulp.watch("source/js/*.js", ["js"]);
});

gulp.task("serve_dev", function () {
  server.init({
    server: "source",
    notify: false,
    open: true,
    cors: true,
    ui: false
  });
  gulp.watch("source/styles/**/*.scss", ["style_dev"]);
  gulp.watch("source/**/*.html", ["copyhtml"]);
  gulp.watch("source/js/*.js", ["js_dev"]);
});

gulp.task("build", function (done) {
  run(
    "style",
    "js",
    "copyhtml",
    done
  );
});

gulp.task("prod", function (done) {
  run(
    "clean",
    "copy",
    "style",
    "js",
    "copyhtml",
    "images",
    done
  );
});
